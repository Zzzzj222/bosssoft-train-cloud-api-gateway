package com.sd365.gateway.authorization.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.gateway.authorization.entity.Resource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ResourceMapper extends CommonMapper<Resource> {
    List<Resource> commonResource();
}
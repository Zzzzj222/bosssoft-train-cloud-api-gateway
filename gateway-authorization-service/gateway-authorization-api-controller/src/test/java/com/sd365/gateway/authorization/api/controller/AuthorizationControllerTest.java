/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName AuthorizationControllerTest.java
 * @Author Administrator
 * @Date 2022-10-13  14:19
 * @Description 文件对提供鉴权服务调用的鉴权接口做单元测试
 * History:
 * <author> Administrator
 * <time> 2022-10-13  14:19
 * <version> 1.0.0
 * <desc> 该文件主要对用户发起请求的鉴权以及某通用不需要鉴权的资源查询的测试用例进行测试，要求
 *  验收程序通过 功能测试+单元测试的执行结果综合评定开发者的开发成果
 */
package com.sd365.gateway.authorization.api.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
/**
 * @Class AuthorizationControllerTest
 * @Description 针对控制器接口的单元测试类，选择了部分接口进行单元测试，开发人员必须按照模板
 * 完成单元测试，后续代码审查中重点审查单元测试是否能达到模板要求
 * @Author Administrator
 * @Date 2022-10-13  14:19
 * @version 1.0.0
 */
class AuthorizationControllerTest {

    /**
     * @Description: 测试鉴权接口能否对方法请求进行鉴权拦截
     * @Author: Administrator
     * @DATE: 2022-10-13  14:19
     */
    @Test
    void roleAuthorization() {
        // 正用例
            //测试步骤1
              // 传入 某一个用户的token 以及 该用户角色资源包含一个 URL（从resource表取得） 返回true
        //反用例
            //测试步骤2
            // 传入 某一个用户的token 以及 该用户角色资源没有包含的一个 返回 false
            //测试步骤3
              //传入一个 不符合规则的token 以及 任意一个URL 系统进行一场处理返回 false
           // 测试步骤4
               // 传入空的参数 token 和 url 系统校验不通过抛出异常

    }
    /**
     * @Description: 测试通过通用资源判断接口测试鉴权服务是否可以放行通用资源的请求
     * @Author: Administrator
     * @DATE: 2022-10-13  14:19
     */
    @Test
    void commonResource() {
        //测试步骤1
           //传入一个 nacos配置文件定义的URL 返回ture
        //测试步骤2
          //传入一个 nacos配置文件没有定义的URL 返回false
        //测试步骤3
        //传入一个空的URL 检验不通过抛出异常

    }
}
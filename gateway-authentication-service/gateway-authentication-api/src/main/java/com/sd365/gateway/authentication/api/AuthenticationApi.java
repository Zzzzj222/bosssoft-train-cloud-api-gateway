package com.sd365.gateway.authentication.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @author yanduohuang
 * @version 1.0
 * @date 2020/12/17 20:47
 */
@Api(tags = "认证管理", value = "/auth")
@RequestMapping("/auth")
@CrossOrigin
public interface AuthenticationApi {
    /**
     * @Description: TODO
     * @Author: Administrator
     * @DATE: 2022-10-12  20:15
     * @param: 
     * @return: CommonResponse<UserVO> 的 JSON String 类型
     */
    @ApiOperation(tags = "获取token", value = "/token")
    @GetMapping("token")
    @CrossOrigin
    String getToken(@RequestParam("code") String code,
                    @RequestParam("account") String account,
                    @RequestParam("password") String password);
}

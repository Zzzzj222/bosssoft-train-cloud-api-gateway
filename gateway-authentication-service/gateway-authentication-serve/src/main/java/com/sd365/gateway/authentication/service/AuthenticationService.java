package com.sd365.gateway.authentication.service;
/**
 * @Class AuthenticationService
 * @Description 认证服务接口，判断用户存在的逻辑在用户中心做，但是JWT规范生成token在认证服务做
 * @Author Administrator
 * @Date 2022-10-12  20:05
 * @version 1.0.0
 */
public interface AuthenticationService {
    int LOGIN_VERIFY_CODE_SUCCESS=1;
    /**
     * @Description: TODO
     * @Author: Administrator
     * @DATE: 2022-10-12  20:05
     * @param: 登录的账号，密码，租户
     * @return:  JWT token
     */
    String getToken(String code, String account, String password);
}

package com.sd365.gateway.core.bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserBo implements Serializable {
    private String code;
    private String password;
    private String type;
    private String tenantId;
    private String blackOrWhiteAccount;
    private String blackOrWhiteName;

    public UserBo(String code, String password, String type, String tenantId) {
        this.code = code;
        this.password = password;
        this.type = type;
        this.tenantId = tenantId;
    }


}
